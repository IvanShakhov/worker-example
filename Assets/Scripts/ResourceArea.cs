using System.Collections.Generic;
using UnityEngine;

namespace GameCore
{
    public class ResourceArea : MonoBehaviour
    {
        [field: SerializeField] private EnumAreaType AreaType { get; set; }
        [field: SerializeField] private List<EnumObjectType> AreaInteractionObjectTypes { get; set; }
        [field: SerializeField] private EnumItemType ItemType { get; set; }
        [field: SerializeField] private float InteractionDelay { get; set; }
        private float _currentDelay;

        private void Update()
        {
            if (_currentDelay > 0)
                _currentDelay -= Time.deltaTime;
        }

        private void OnTriggerStay(Collider other)
        {
            if (_currentDelay <= 0)
                foreach (var objectType in AreaInteractionObjectTypes)
                {
                    if (other.gameObject.tag == objectType.ToString())
                    {
                        if (AreaType is EnumAreaType.TakeResource)
                        {
                            other.GetComponent<Bag>()?.AddItem(ItemType);
                        }
                        else if (AreaType is EnumAreaType.GiveResource)
                        {
                            other.GetComponent<Bag>()?.RemoveItem(ItemType);
                        }

                        _currentDelay = InteractionDelay;
                    }
                }

        }
    }
}


