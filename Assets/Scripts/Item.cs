using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameCore
{
    public class Item : MonoBehaviour
    {
        [field: SerializeField] public EnumItemType ItemType { get; private set; }
        [field: SerializeField] public float ItemValue { get; private set; }
        [field: SerializeField] public bool IsDelete { get; private set; }

        public void Delete()
        {
            IsDelete = true;
            Destroy(gameObject);
        }
    }
}
