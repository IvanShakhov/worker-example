using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameCore
{
    
    public class CameraController : MonoBehaviour
    {
        [field: SerializeField] private GameObject Player { get; set; }

        [field: SerializeField] private Camera Camera { get; set; }

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            Camera.transform.position = Vector3.Lerp(Camera.transform.position,
                Player.transform.position + new Vector3(0f, 13f, -5f), 0.2f);
        }
    }
}
