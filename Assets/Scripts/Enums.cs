namespace GameCore
{
    public enum EnumAreaType
    {
        None,
        TakeResource,
        GiveResource
    }

    public enum EnumObjectType
    {
        None,
        Player
    }

    public enum EnumItemType
    {
        None,
        Gold
    }
}
