using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

namespace GameCore
{
    public class Bag : MonoBehaviour
    {
        [field: SerializeField] private List<Item> Items { get; set; }
        [field:SerializeField] private Transform TransformItemsParent { get; set; }

        public void AddItem(EnumItemType itemType)
        {
            foreach (var item in ManagerData.Instance.Items)
            {
                if (item.ItemType == itemType)
                {
                    var tempObject=Instantiate(item, TransformItemsParent);
                    Items.Add(tempObject);
                    break;
                }
            }
            SortItems();

        }

        public void RemoveItem(EnumItemType itemType)
        {
            foreach (var item in Items)
            {
                if (item.ItemType == itemType)
                {
                    item.Delete();
                    break;
                }
            }

            Items.RemoveAll(item => item.IsDelete);
            SortItems();
        }

        private void SortItems()
        {
            int column = 0;
            int row = 0;
            Vector3 spaceColumn = new Vector3(0.3f, 0f, 0f);
            Vector3 spaceRow = new Vector3(0f, 0.2f, 0f);
            for (int i = 0; i < Items.Count; i++)
            {
                column = (i - i % 20) / 20;
                row = i % 20;
                Items[i].transform.localPosition = column * spaceColumn + row * spaceRow;
            }
        }
    }
}
