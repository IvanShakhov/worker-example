using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace GameCore
{
    public class ManagerData : MonoBehaviour
    {
        [field: SerializeField] public List<Item> Items { get; private set; }
        public static ManagerData Instance { get; private set; }

        private void Awake()
        {
            Instance = this;
        }
    }
}
