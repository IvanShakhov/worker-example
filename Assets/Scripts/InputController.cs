using UnityEngine;

namespace GameCore
{
    public class InputController : MonoBehaviour
    {
        [field:SerializeField] private CharacterController Controller { get; set; }
        private float playerSpeed = 10.0f;


        void Update()
        {
            Vector3 move = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
            Controller.Move(move * Time.deltaTime * playerSpeed);
        }
    }
}
